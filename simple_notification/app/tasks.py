import os
import json
import requests
from requests import Response

from celery import shared_task
from django.utils.timezone import now
from django.db.models.expressions import Q

from app.models import NotificationList, Message, Client
from simple_notification.settings import SENDER_URL
from simple_notification.settings import SENDING_INTERVAL, SENDING_RETRY_INTERVAL


@shared_task(bind=True, name="msg_sender", max_retries=None)
def task_send_notification(self, nl_id: int, client_id: int, r=0):
    token = os.environ.get("TOKEN")
    nl, created = NotificationList.objects.get_or_create(pk=nl_id)
    client, created = Client.objects.get_or_create(pk=client_id)
    msg, created = Message.objects.get_or_create(
        notification_list=nl, client=client
    )
    url = SENDER_URL + str(msg.pk)
    try:
        response: Response = requests.post(
            url,
            headers={"Authorization": f"Bearer {token}"},
            json={
                "id": msg.pk,
                "phone": client.phone_number,
                "text": nl.message_text,
            },
        )
    except Exception:
        self.retry(countdown=10)

    if response.status_code == 200:
        content = json.loads(response.content)
        if "code" in content.keys() and "message" in content.keys():
            if content["code"] == 0 and content["message"] == "OK":
                Message.objects.update(
                    datetime_sended=now().isoformat(), status_sended=True
                )
                return msg.pk
        else:
            self.retry(countdown=10)
    else:
        self.retry(countdown=10)


@shared_task(name="nl_sender")
def task_send_notification_list(nl_id: int):
    condition = Q()
    instance, created = NotificationList.objects.get_or_create(pk=nl_id)
    for el in instance.filter_target.split(", "):
        condition |= Q(tag__exact=el)
        condition |= Q(mobile_operator__exact=el)
    clients_to_notify = Client.objects.filter(condition)
    for client in clients_to_notify:
        msg, created = Message.objects.get_or_create(
            notification_list=instance, client=client
        )
        if msg and not msg.status_sended:
            expire_dt = NotificationList.objects.get(pk=nl_id).datetime_end
            task = task_send_notification.s(nl_id, client.pk)
            if expire_dt:
                task.apply_async(expires=(expire_dt - now()).total_seconds())
            else:
                task.apply_async()
