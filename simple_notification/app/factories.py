import factory.fuzzy

from django.utils.timezone import now, timedelta
from faker import Faker
from random import randint, choice

from simple_notification import settings


Faker.seed(0)
fake = Faker([settings.LANGUAGE_CODE])
fake._DEFAULT_LOCALE = settings.LANGUAGE_CODE
factory.Faker._DEFAULT_LOCALE = settings.LANGUAGE_CODE


class ClientFactory(factory.django.DjangoModelFactory):
    phone_number = factory.LazyFunction(
        lambda: "7" + str(randint(1000000000, 9999999999))
    )
    mobile_operator = factory.LazyFunction(
        lambda: choice(["mts", "megafon", "tele2", "beeline", "yota"])
    )
    tag = factory.Faker("word")

    class Meta:
        model = "app.Client"
        django_get_or_create = ("phone_number",)


class NotificationListFactory(factory.django.DjangoModelFactory):
    datetime_start = factory.LazyFunction(
        lambda: now() + timedelta(randint(0, 100000))
    )
    datetime_end = factory.LazyAttribute(
        lambda o: o.datetime_start + timedelta(randint(1000, 100000))
    )
    message_text = factory.Faker("text")
    filter_target = factory.fuzzy.FuzzyChoice(
        choices=["mts", "megafon", "tele2", "beeline", "yota"]
    )

    class Meta:
        model = "app.NotificationList"
