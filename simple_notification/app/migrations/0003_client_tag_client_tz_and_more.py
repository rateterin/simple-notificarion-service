# Generated by Django 4.0.7 on 2022-08-24 07:42

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("app", "0002_alter_notificationlist_filter_target"),
    ]

    operations = [
        migrations.AddField(
            model_name="client",
            name="tag",
            field=models.CharField(blank=True, default="", max_length=16),
        ),
        migrations.AddField(
            model_name="client",
            name="tz",
            field=models.CharField(default="0", max_length=7),
        ),
        migrations.AlterField(
            model_name="notificationlist",
            name="filter_target",
            field=models.CharField(
                help_text="sss, sss, sss ... (sss - world (a-z, A-Z, а-я, А-Я -, _))",
                max_length=64,
                validators=[
                    django.core.validators.RegexValidator(
                        regex="^([\\d\\w-]*)(, [\\d\\w-]*)*$"
                    )
                ],
            ),
        ),
    ]
