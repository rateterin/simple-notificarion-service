from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.timezone import now

from simple_notification.celery import celery_app
from celery.signals import task_success, task_retry

from app.models import NotificationList, Client
from app.tasks import task_send_notification_list


@receiver(post_save, sender=NotificationList)
def signal_notification_list_post_save(sender, instance, created, **kwargs):
    dt_now = now()
    task = task_send_notification_list.s(instance.pk)
    if instance.datetime_start < dt_now:
        task.apply_async()
    else:
        countdown = instance.datetime_start - dt_now
        countdown = countdown.total_seconds()
        task.apply_async(countdown=countdown)


@receiver(post_save, sender=Client)
def signal_post_save_client_restart_director(
    sender, instance, created, **kwargs
):
    pass


@task_success.connect()
def succes_task_logger(sender=None, result=None, *args, **kwargs):
    if sender == "msg_sender":
        print(f"DEBUG: msg {result} sended success")


@task_retry.connect()
def retry_task_logger(
    sender=None, request=None, reason=None, einfo=None, *args, **kwargs
):
    if sender == "msg_sender":
        print(
            f"DEBUG: task {request.id} msg {celery_app.tasks[request.id].msg.pk} restart {reason}.."
        )
