from rest_framework import serializers

from app.models import NotificationList, Client, Message


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = "__all__"


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = "__all__"


class NotificationListSerializer(serializers.ModelSerializer):
    class Meta:
        model = NotificationList
        fields = [
            "id",
            "datetime_start",
            "datetime_end",
            "message_text",
            "filter_target",
            "messages_sended_ok",
            "messages_sended_error",
        ]
        extra_kwargs = {
            "messages_sended_ok": {
                "source": "get_sended_msg_count",
                "read_only": True,
            },
            "messages_sended_error": {
                "source": "get_not_sended_msg_count",
                "read_only": True,
            },
        }
