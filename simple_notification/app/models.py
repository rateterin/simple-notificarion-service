from django.db import models
from django.utils.timezone import now

from app.validators import comma_separated_worlds, phone_number


class Client(models.Model):
    phone_number = models.CharField(
        validators=[phone_number],
        max_length=11,
        unique=True,
        help_text="7xxxxxxxxxx (x - digit 0 - 9)",
    )
    mobile_operator = models.CharField(
        max_length=16, blank=False, default="Unknown"
    )
    tag = models.CharField(max_length=16, blank=True, default="")
    tz = models.CharField(max_length=7, blank=False, null=False, default="0")


class NotificationList(models.Model):
    datetime_start = models.DateTimeField(
        null=False, default=now, help_text=now
    )
    datetime_end = models.DateTimeField(null=True, help_text=now)
    message_text = models.TextField(
        max_length=1024, blank=True, null=False, default=""
    )
    filter_target = models.CharField(
        validators=[comma_separated_worlds],
        max_length=64,
        help_text="sss, sss, sss ... (sss - world (a-z, A-Z, а-я, А-Я -, _))",
    )

    def get_sended_msg_count(self):
        return Message.objects.filter(
            notification_list=self.pk, status_sended=True
        ).count()

    def get_not_sended_msg_count(self):
        return Message.objects.filter(
            notification_list=self.pk, status_sended=False
        ).count()


class Message(models.Model):
    datetime_created = models.DateTimeField(auto_now_add=True)
    datetime_sended = models.DateTimeField(null=True, default=None)
    status_sended = models.BooleanField(default=False)
    notification_list = models.ForeignKey(
        "app.NotificationList",
        on_delete=models.DO_NOTHING,
        related_name="messages",
    )
    client = models.ForeignKey(
        "app.Client", on_delete=models.DO_NOTHING, related_name="messages"
    )
