from django.core.validators import RegexValidator


comma_separated_worlds = RegexValidator(regex=r"^([\d\w-]*)(, [\d\w-]*)*$")
phone_number = RegexValidator(regex=r"^7\d{10}$")
