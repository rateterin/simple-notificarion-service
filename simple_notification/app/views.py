from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, mixins

from app.models import Client, NotificationList, Message
from app.serializers import (
    ClientSerializer,
    NotificationListSerializer,
    MessageSerializer,
)


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class NotificationListViewSet(
    mixins.ListModelMixin,
    mixins.CreateModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    viewsets.GenericViewSet,
):
    queryset = NotificationList.objects.all()
    serializer_class = NotificationListSerializer


class MessageViewSet(
    mixins.ListModelMixin, mixins.RetrieveModelMixin, viewsets.GenericViewSet
):
    queryset = Message.objects.all().order_by("pk")
    serializer_class = MessageSerializer

    def get_queryset(self):
        if self.action == "retrieve":
            return (
                super()
                .get_queryset()
                .filter(notification_list=self.kwargs.get("pk"))
            )
        return super().get_queryset()
