from django.utils.timezone import now, timedelta
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from app.factories import ClientFactory, NotificationListFactory
from app.models import Client, NotificationList


class TestCaseWithData(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.client_url = reverse("client-list", kwargs={"format": "json"})
        cls.notification_list_url = reverse("notificationlist-list")
        client = ClientFactory.build()
        cls.client_data = {
            "phone_number": client.phone_number,
            "mobile_operator": client.mobile_operator,
            "tag": client.tag,
            "tz": client.tz,
        }
        cls.client1 = ClientFactory()
        cls.notification_list_nactive = NotificationListFactory.create(
            datetime_start=now() - timedelta(weeks=2),
            datetime_end=now() - timedelta(weeks=1),
            filter_target=[client.mobile_operator],
        )
        cls.notification_list_active = NotificationListFactory.build(
            datetime_start=now(),
            datetime_end=now() + timedelta(seconds=10),
            filter_target=[client.mobile_operator],
        )
        cls.notification_list_data = {
            "datetime_start": cls.notification_list_active.datetime_start,
            "datetime_end": cls.notification_list_active.datetime_end,
            "message_text": cls.notification_list_active.message_text,
            "filter_target": cls.notification_list_active.filter_target,
        }
        cls.message_url = reverse("message-list", kwargs={"format": "json"})


class TestCaseClient(TestCaseWithData):
    def setUp(self):
        pass

    def test_create_client(self):
        with self.assertNumQueries(2):
            response = self.client.post(
                self.client_url,
                data=self.client_data,
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        client_pk = response.json().get("id")

        self.assertEqual(Client.objects.filter(pk=client_pk).exists(), True)

    def test_get_client(self):
        url = reverse(
            "client-detail", format="json", kwargs={"pk": self.client1.pk}
        )
        with self.assertNumQueries(1):
            response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 5)

    def test_list_client(self):
        with self.assertNumQueries(1):
            response = self.client.get(self.client_url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response.json(), list)
        self.assertEqual(len(response.json()), 1)
        client = response.json()[0]
        self.assertIsInstance(client.get("id"), int)
        self.assertIsInstance(client.get("phone_number"), str)
        self.assertIsInstance(client.get("mobile_operator"), str)
        self.assertIsInstance(client.get("tag"), str)
        self.assertIsInstance(client.get("tz"), str)

    def test_patch_client(self):
        url = reverse(
            "client-detail", format="json", kwargs={"pk": self.client1.pk}
        )
        with self.assertNumQueries(2):
            response = self.client.patch(url, {"tag": "test_tag"})

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response.json(), dict)
        self.assertEqual(len(response.json()), 5)
        client = response.json()
        self.assertIsInstance(client.get("id"), int)
        self.assertIsInstance(client.get("phone_number"), str)
        self.assertIsInstance(client.get("mobile_operator"), str)
        self.assertIsInstance(client.get("tag"), str)
        self.assertIsInstance(client.get("tz"), str)

        self.assertEqual(client.get("tag"), "test_tag")

    def test_get_client(self):
        url = reverse(
            "client-detail", format="json", kwargs={"pk": self.client1.pk}
        )
        with self.assertNumQueries(2):
            response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(
            Client.objects.filter(pk=self.client1.pk).exists(), False
        )


class TestCaseNotificationList(TestCaseWithData):
    def setUp(self):
        pass

    def test_create_notification_list(self):
        with self.assertNumQueries(3):
            response = self.client.post(
                self.notification_list_url,
                data=self.notification_list_data,
            )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        nl_pk = response.json().get("id")

        self.assertEqual(
            NotificationList.objects.filter(pk=nl_pk).exists(), True
        )

    def test_get_notification_list(self):
        url = reverse(
            "notificationlist-detail",
            format="json",
            kwargs={"pk": self.notification_list_nactive.pk},
        )
        with self.assertNumQueries(1):
            response = self.client.get(url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 5)

    def test_list_notification_list(self):
        with self.assertNumQueries(3):
            response = self.client.get(self.notification_list_url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response.json(), list)
        self.assertEqual(len(response.json()), 1)
        client = response.json()[0]
        self.assertIsInstance(client.get("id"), int)
        self.assertIsInstance(client.get("datetime_start"), str)
        self.assertIsInstance(client.get("datetime_end"), str)
        self.assertIsInstance(client.get("message_text"), str)
        self.assertIsInstance(client.get("filter_target"), str)

    def test_patch_notification_list(self):
        url = reverse(
            "notificationlist-detail",
            format="json",
            kwargs={"pk": self.notification_list_nactive.pk},
        )
        with self.assertNumQueries(4):
            response = self.client.patch(url, {"filter_target": "test_tag"})

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsInstance(response.json(), dict)
        self.assertEqual(len(response.json()), 7)
        client = response.json()
        self.assertIsInstance(client.get("id"), int)
        self.assertIsInstance(client.get("datetime_start"), str)
        self.assertIsInstance(client.get("datetime_end"), str)
        self.assertIsInstance(client.get("message_text"), str)
        self.assertIsInstance(client.get("filter_target"), str)
        self.assertIsInstance(client.get("messages_sended_ok"), int)
        self.assertIsInstance(client.get("messages_sended_error"), int)

        self.assertEqual(client.get("filter_target"), "test_tag")

    def test_get_notification_list(self):
        url = reverse(
            "notificationlist-detail",
            format="json",
            kwargs={"pk": self.notification_list_nactive.pk},
        )
        with self.assertNumQueries(2):
            response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(
            Client.objects.filter(
                pk=self.notification_list_nactive.pk
            ).exists(),
            False,
        )
