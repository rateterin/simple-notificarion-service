#!/usr/bin/env sh

python manage.py collectstatic --noinput
python manage.py migrate --noinput

hypercorn simple_notification.asgi:application --workers 2 --bind "0.0.0.0:8000"
