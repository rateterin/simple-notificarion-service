# Simple notification service

Also, please read ТЗ.md

Instruction to start project.
=============================

Make .env from .env.example by copy and type your data in it.

For local start tests or development - please change DEBUG to true, change 
DOMAIN, POSTGRES_HOST and REDIS_HOST to localhost 
and up database with executing in terminal from folder, containing docker-compose*.yml files, 
command "docker-compose -f docker-compose_local_db.yml up" (first start take more time)

To start tests execute in terminal from folder, containing manage.py "python3 manage.py test"

To start project execute in terminal from folder, containing docker-compose*.yml files, 
command "docker-compose up -d" (first start take more time)
